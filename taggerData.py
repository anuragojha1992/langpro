from pymongo import MongoClient
from pymongo import errors


class TaggerDataStore:
    conString = "http://locahost:27107"

    def __init__(self):
        try:
            self.client = MongoClient('mongodb://localhost:27017/')
            self.db = self.client['tagger']
        except errors.ConnectionFailure:
            print("An exception occurred whilst connecting")

    def save(self, collection, document):
        try:
            self.db[collection].insert(document)
            return True
        except errors.OperationFailure:
            print("An exception occurred whilst saving")
            return False

    def saveMany(self, collection, documents):
        try:
            self.db[collection].insert_many(documents)
            return True
        except errors.OperationFailure:
            print("An exception occurred whilst saving")
            return False

    def get(self, collection):
        try:
            results = []
            dbres = self.db[collection].find()
            for res in dbres:
                results.append(res)
            return results
        except errors.OperationFailure:
            print("An exception occurred whilst fetching data.")
            return False

    def getOne(self, collection):
        try:
            return self.db[collection].find_one()
        except errors.OperationFailure:
            print("An exception occurred whilst fetching data.")
            return False



        except errors.OperationFailure:
            print("An exception occurred whilst fetching")
            return {}

    def getWithFilter(self, collection, filter):

        try:
            return self.db[collection].find(filter)
        except errors.OperationFailure:
            print("An exception occurred whilst fetching")
            return []

    def close(self):

        self.client.close()
