from tagger import Tagger
import re
from sentiment import Sentiment


def get_preferences(reviews):
    preference = dict()
    preference["pos"] = []
    preference["neg"] = []
    for review in reviews:
        sentences = review.split(".")
        for sentence in sentences:
            sentence = re.sub('[^A-Za-z0-9 ]+', '', sentence)
            pos = Tagger.predict(sentence)
            sentiment = Sentiment.predict(sentence)
            for key in pos:
                if pos[key] == "NOUN":
                    preference[sentiment].append(key)

    return preference
