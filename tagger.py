from nltk.corpus import brown
from taggerData import TaggerDataStore
from nltk.tokenize import word_tokenize
from collections import Counter
from collections import defaultdict
import numpy


class Tagger:
    universalTags = ["ADJ", "ADP", "ADV", "AUX", "CONJ", "DET", "INTJ", "NOUN", "NUM", "PRT", "PRON", "PROPN", "PUNCT",
                     "SCONJ", "SYM", "VERB", "X", "DOT"]

    @staticmethod
    def train():
        data_store = TaggerDataStore()

        # Fetching all sentences from the corpus
        clean_tagged_sentences = Tagger.clean_sentences(brown.tagged_sents(tagset="universal"))
        clean_tagged_words = Tagger.clean_words(brown.tagged_words(tagset="universal"))
        tags_count = Counter(word[1] for word in clean_tagged_words)  # C(ti)
        tagword_count = Counter(word for word in clean_tagged_words)  # C(ti,wi)

        # Calculating Emission Matrix
        emMatrix = defaultdict(dict)
        for pair in tagword_count:
            emMatrix[pair[1]][pair[0]] = tagword_count[pair] / (tags_count[pair[1]] + 1)

        # Calculating Transition Matrix
        transMatrix = defaultdict(dict)
        tag_pairs = []

        # Get all tag pairs.
        for sentence in clean_tagged_sentences:
            sentence_tags = [pair[1] for pair in sentence]
            sentence_tags_pairs = [(sentence_tags[i], sentence_tags[i + 1]) for i in range(len(sentence_tags) - 1)]
            tag_pairs = tag_pairs + sentence_tags_pairs

        tag_pairs_count = Counter(pair for pair in tag_pairs)
        for pair in tag_pairs_count:
            transMatrix[pair[1]][pair[0]] = tag_pairs_count[pair] / (tags_count[pair[1]] + 1)

        # Used in viterbi
        for tag in Tagger.universalTags:
            transMatrix["START"][tag] = 1.0
            transMatrix[tag]["END"] = 1.0

        data_store.save("EmissionMatrix", emMatrix)
        data_store.save("TransmissionMatrix", transMatrix)
        data_store.close()

    @staticmethod
    def predict(sentence):
        dataStore = TaggerDataStore()
        emMatrix = dataStore.getOne("EmissionMatrix")
        transMatrix = dataStore.getOne("TransmissionMatrix")

        return Tagger.viterbi(sentence, transMatrix, emMatrix)

    @staticmethod
    def clean_sentences(tagged_sentences):
        clean_tagged_sentences = []
        for taggedWords in tagged_sentences:
            clean_tag_words = []
            # Reformat Data for  Mongo
            for word, tag in taggedWords:
                clean_tag_words.append(
                    (word.replace('.', "DOT").replace('$', "DOLLAR").lower(),
                     tag.replace('.', "DOT").replace('$', "DOLLAR")))
            clean_tagged_sentences.append(clean_tag_words)
        return clean_tagged_sentences

    @staticmethod
    def clean_words(tagged_words):
        clean_tag_words = []
        # Reformat Data for  Mongo
        for word, tag in tagged_words:
            clean_tag_words.append(
                (word.replace('.', "DOT").replace('$', "DOLLAR").lower(),
                 tag.replace('.', "DOT").replace('$', "DOLLAR")))

        return clean_tag_words

    def viterbi(sentence, transMatrix, emMatrix):
        tokenizedword = word_tokenize(sentence, language='english')

        # x = map(self.smap.get, x)  # turn	emission	characters	into	ids
        nrow, ncol = len(Tagger.universalTags), len(tokenizedword)
        mat = numpy.zeros(shape=(nrow, ncol), dtype=float)  # prob
        matTb = numpy.zeros(shape=(nrow, ncol), dtype=int)  # backtrace

        #	Fill	in	first	column
        for i in range(0, nrow):
            mat[i, 0] = emMatrix.get(Tagger.universalTags[i], {}).get(tokenizedword[0], 0)
        # Fill	in	rest	of	prob	and	Tb	tables
        for j in range(1, ncol):
            for i in range(0, nrow):
                ep = emMatrix.get(Tagger.universalTags[i], {}).get(tokenizedword[j], 0)
                mx, mxi = mat[0, j - 1] * transMatrix.get(Tagger.universalTags[0], {}).get(Tagger.universalTags[i],
                                                                                           0) * ep, 0
                for i2 in range(1, nrow):
                    pr = mat[i2, j - 1] * transMatrix.get(Tagger.universalTags[i2], {}).get(Tagger.universalTags[i],
                                                                                            0) * ep
                    if pr > mx:
                        mx, mxi = pr, i2
                mat[i, j], matTb[i, j] = mx, mxi
        # Find	final	state	with	maximal	probability
        omx, omxi = mat[0, ncol - 1], 0
        for i in range(1, nrow):
            if mat[i, ncol - 1] > omx:
                omx, omxi = mat[i, ncol - 1], i

        # Backtrace
        i, p = omxi, [omxi]
        for j in range(ncol - 1, 0, -1):
            i = matTb[i, j]
            p.append(i)

        p.reverse()

        results = dict()
        for i in range(0, len(p)):
            results[tokenizedword[i]] = Tagger.universalTags[p[i]]

        return results
