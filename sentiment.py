import xlrd
from nltk.corpus import stopwords
import itertools
from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from taggerData import TaggerDataStore
from collections import defaultdict
import re
import nltk.data
import random


try:
    import cPickle as pickle
except:
    import pickle


class Sentiment:
    @staticmethod
    def removeStopWords(wordlist):
        stop = stopwords.words('english')
        return [w for w in wordlist if w not in stop]

    @staticmethod
    def wordListToFreqDict(wordlist):
        wordfreq = [wordlist.count(p) for p in wordlist]
        return dict(zip(wordlist, wordfreq))

    @staticmethod
    def bigram_word_feats(words, score_fn=BigramAssocMeasures.chi_sq, n=10000):
        bigram_finder = BigramCollocationFinder.from_words(words)
        bigrams = bigram_finder.nbest(score_fn, n)
        return dict([(ngram, True) for ngram in itertools.chain(words, bigrams)])

    @staticmethod
    def train_review():
        data_store = TaggerDataStore()
        # data = xlrd.open_workbook("/Users/ansingla/Desktop/testdata_manual.xls")
        data = xlrd.open_workbook("/Users/ansingla/Desktop/pos/training.xlsx")
        sheet = data.sheet_by_index(0)
        bigrams_pos_count = 0
        unigram_pos_count = 0
        bigram_neg_count = 0
        unigram_neg_count = 0
        bigram_neutral_count = 0
        unigram_neutral_count = 0
        bigrams_pos = []
        bigrams_neg = []
        bigrams_neutral = []
        for row in range(sheet.nrows):
            #  sentence = ''.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ",
            #                           sheet.cell(row, 5).value).split()).split()
            sentence = sheet.cell(row, 5).value
            sentence = sentence.split()
            bigrams = Sentiment.bigram_word_feats(sentence)

            cell = sheet.cell(row, 0).value
            if cell == 4.0:
                bigrams_pos_count = bigrams_pos_count + len([x for x in bigrams.keys() if type(x) is tuple])
                unigram_pos_count = unigram_pos_count + len([x for x in bigrams.keys() if type(x) is str])
                bigrams_pos = bigrams_pos + list(bigrams.keys())
            elif cell == 0.0:
                bigram_neg_count = bigram_neg_count + len([x for x in bigrams.keys() if type(x) is tuple])
                unigram_neg_count = unigram_neg_count + len([x for x in bigrams.keys() if type(x) is str])
                bigrams_neg = bigrams_neg + list(bigrams.keys())
            else:
                bigram_neutral_count = bigram_neutral_count + len([x for x in bigrams.keys() if type(x) is tuple])
                unigram_neutral_count = unigram_neutral_count + len([x for x in bigrams.keys() if type(x) is str])
                bigrams_neutral = bigrams_neutral + list(bigrams.keys())

        bigrams_poscount = Sentiment.wordListToFreqDict(bigrams_pos)
        bigrams_negcount = Sentiment.wordListToFreqDict(bigrams_neg)
        bigrams_neutralcount = Sentiment.wordListToFreqDict(bigrams_neutral)

        positive = defaultdict(dict)
        negative = defaultdict(dict)
        neutral = defaultdict(dict)

        for i in bigrams_poscount:
            total_pos = bigrams_pos_count if type(positive[i] == 'tuple') else unigram_pos_count
            positive[i] = ((bigrams_poscount[i] + 1) / (total_pos + 1))
        for i in bigrams_negcount:
            total_neg = bigram_neg_count if type(negative[i] == 'tuple') else unigram_neg_count
            negative[i] = ((bigrams_negcount[i] + 1) / (total_neg + 1))
        for i in bigrams_neutralcount:
            total_neutral = bigram_neutral_count if type(neutral[i] == 'tuple') else unigram_neutral_count
            neutral[i] = ((bigrams_neutralcount[i] + 1) / (total_neutral + 1))

        data_store.save("PosProbability", {"positive": pickle.dumps(positive)})
        data_store.save("NegProbability", {"negative": pickle.dumps(negative)})
        data_store.save("NeutralProbability", {"neutral": pickle.dumps(neutral)})
        data_store.close()

    @staticmethod
    def predict(para):
        dataStore = TaggerDataStore()
        positive = pickle.loads(dataStore.getOne("PosProbability")["positive"])
        negative = pickle.loads(dataStore.getOne("NegProbability")["negative"])
        neutral = pickle.loads(dataStore.getOne("NeutralProbability")["neutral"])
        posscore = len(positive) / (len(positive) + len(negative) + len(neutral))
        negscore = len(negative) / (len(positive) + len(negative) + len(neutral))
        neutralscore = len(neutral) / (len(positive) + len(negative) + len(neutral))
        posDefault = 1 / (len(positive) + 1)
        negDefault = 1 / (len(negative) + 1)
        neutralDefault = 1 / (len(neutral) + 1)
        sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
        sentence = sent_detector.tokenize(para.strip())
        score = 0.0
        for i in range(0, (len(sentence))):
            currentsentence = sentence[i].split()
            bigrams = Sentiment.bigram_word_feats(currentsentence)
            for key in bigrams:
                posscore = posscore * positive.get(key, posDefault)
                negscore = negscore * negative.get(key, negDefault)
                neutralscore = neutralscore * neutral.get(key, neutralDefault)
            if posscore > negscore:
                if posscore > neutralscore:
                    num = random.uniform(3,5)
                    score = score + 4*num
                else:
                    num = random.randint(1, 3)
                    score = score + 2*num
            else:
                if negscore > neutralscore:
                    score = score + 0*3
                else:
                    num = random.randint(1, 3)
                    score = score + 2 * num

    @staticmethod
    def test():
        correct = 0
        total = 0
        #  data = xlrd.open_workbook("/Users/ansingla/Desktop/training.xlsx")
        data = xlrd.open_workbook("/Users/ansingla/Desktop/pos/testdata_manual.xls")
        sheet = data.sheet_by_index(0)
        for row in range(sheet.nrows):
            # sentence = ''.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ",
            #                         sheet.cell(row, 5).value).split()).split()
            sentence = sheet.cell(row, 5).value
            # print(sentence)
            sentiment = Sentiment.predict(sentence)
            if sentiment == sheet.cell(row, 0).value:
                correct = correct + 1
                total = total + 1
            else:
                total = total + 1
        percentage = (correct / total) * 100
        print(percentage)
